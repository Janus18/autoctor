# AutoCtor
FsCheck is able to derive an Arbitrary of a given class with Arb.Default.Derive,<br>
but it does not provide a way to specify how its members are generated.

For example, if we have the following class

    public class X
    {
        public int NotNegative { get; set; }

        public Y Navigation { get; set; }
    }

We have no way to generate *X* objects where *NotNegative* is always a positive integer,<br>
or where *Navigation* is always null.

This attempts to provide an easy way to achieve that by using a simple generator builder for classes,<br>
which uses the default FsCheck generators unless another one is specified for the member.

# Example
We can generate *X* objects which satisfy the constraints mentioned with:

    AutoCtor<X>.Builder()
            .Having(nameof(X.NotNegative), Arb.Default.Int32().Generator.Select(x => System.Math.Abs(x)))
            .HavingConstant(nameof(X.Navigation), Gen.Constant<Y>(null))
            .GetGenerator()
