using FsCheck;

namespace AutoCtor.Tests.Models;

public static class BasicSUTConstrainedInt
{
    public static Arbitrary<BasicSUT> GetArb()
    {
        return AutoCtor.AutoCtor<BasicSUT>.Builder()
            .Having(nameof(BasicSUT.Int), Arb.Default.Int32().Generator.Select(x => System.Math.Abs(x)))
            .GetGenerator()
            .ToArbitrary();
    }
}
