using FsCheck;

namespace AutoCtor.Tests.Models;

public static class FullSUT
{
    public static Gen<SUT> GetGen()
    {
        return AutoCtor.AutoCtor<SUT>.Builder().GetGenerator();
    }

    public static Arbitrary<SUT> GetArb()
    {
        return GetGen().ToArbitrary();
    }
}
