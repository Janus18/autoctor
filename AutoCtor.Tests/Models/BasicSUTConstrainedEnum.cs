using FsCheck;

namespace AutoCtor.Tests.Models;

public static class BasicSUTConstrainedEnum
{
    public static Arbitrary<BasicSUT> GetArb()
    {
        var noState3Gen = Gen.OneOf(
            Gen.Constant(BasicEnum.State1),
            Gen.Constant(BasicEnum.State2)
        );
        return AutoCtor.AutoCtor<BasicSUT>.Builder()
            .Having(nameof(BasicSUT.Enum), noState3Gen)
            .GetGenerator()
            .ToArbitrary();
    }
}
