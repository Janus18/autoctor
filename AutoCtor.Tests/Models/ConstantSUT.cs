using FsCheck;
using System;
using System.Collections.Generic;
using System.Numerics;

namespace AutoCtor.Tests.Models;

public static class ConstantSUT
{
    public static Gen<SUT> GetGen()
    {
        return AutoCtor.AutoCtor<SUT>.Builder()
            .HavingConstant<BigInteger>(nameof(SUT.BigInt))
            .HavingConstant<bool>(nameof(SUT.Bool))
            .HavingConstant<byte>(nameof(SUT.Byte))
            .HavingConstant<Complex>(nameof(SUT.Complx))
            .HavingConstant<DateTime>(nameof(SUT.Dt))
            .HavingConstant<DateTimeOffset>(nameof(SUT.Dto))
            .HavingConstant<decimal>(nameof(SUT.Decimal))
            .HavingConstant<double>(nameof(SUT.Double))
            .HavingConstant<float>(nameof(SUT.Float))
            .HavingConstant<Guid>(nameof(SUT.Guid))
            .HavingConstant<int>(nameof(SUT.Int))
            .HavingConstant<ICollection<decimal>>(nameof(SUT.Decimals))
            .HavingConstant<IEnumerable<string>>(nameof(SUT.Strings))
            .HavingConstant<IList<int>>(nameof(SUT.Ints))
            .HavingConstant<ISet<long>>(nameof(SUT.Longs))
            .HavingConstant<List<float>>(nameof(SUT.Floats))
            .HavingConstant<long>(nameof(SUT.Long))
            .HavingConstant<object>(nameof(SUT.Obj))
            .HavingConstant<short>(nameof(SUT.Short))
            .HavingConstant<string>(nameof(SUT.String))
            .HavingConstant<TimeSpan>(nameof(SUT.Ts))
            .HavingConstant<int[]>(nameof(SUT.ArrInt))
            .HavingConstant<string[]>(nameof(SUT.ArrString))
            .GetGenerator();
    }

    public static Arbitrary<SUT> GetArb()
    {
        return GetGen().ToArbitrary();
    }
}
