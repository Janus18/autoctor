namespace AutoCtor.Tests.Models;

public enum BasicEnum
{
    State1 = 1,
    State2 = 2,
    State3 = 3
}

public class BasicSUT
{
    public BasicEnum Enum { get; set; }

    public int Int { get; set; }
}
