using System;
using System.Collections.Generic;
using System.Numerics;

namespace AutoCtor.Tests.Models;

public class SUT
{
    public BigInteger BigInt { get; set; }

    public bool Bool { get; set; }

    public byte Byte { get; set; }

    public Complex Complx { get; set; }

    public DateTime Dt { get; set; }

    public DateTimeOffset Dto { get; set; }

    public decimal Decimal { get; set; }

    public double Double { get; set; }

    public float Float { get; set; }

    public Guid Guid { get; set; }

    public int Int { get; set; }

    public ICollection<decimal> Decimals { get; set; } = new List<decimal>();

    public IEnumerable<string> Strings { get; set; } = new List<string>();

    public IList<int> Ints { get; set; } = new List<int>();

    public ISet<long> Longs { get; set; } = new HashSet<long>();

    public List<float> Floats { get; set; } = new List<float>();

    public long Long { get; set; }

    public object Obj { get; set; } = new {};

    public short Short { get; set; }

    public string String { get; set; } = "";    

    public TimeSpan Ts { get; set; }

    public int[] ArrInt { get; set; } = Array.Empty<int>();
    
    public string[] ArrString { get; set; } = Array.Empty<string>();
}
