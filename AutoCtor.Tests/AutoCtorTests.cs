using AutoCtor.Tests.Models;
using FluentAssertions;
using FsCheck;
using FsCheck.Xunit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Xunit;

namespace AutoCtor.Tests;

public static class AutoCtorTests
{
    [Property(Arbitrary = new[] { typeof(FullSUT) })]
    public static void FullSUTIsGenerated(SUT sut)
    {
        Assert.NotNull(sut);
    }

    [Fact]
    public static void ForFullSUT_ManyInstancesCanBeGenerated_AndAreDifferentFromEachOther()
    {
        var samples = Enumerable.Range(0, 10).SelectMany(i => FullSUT.GetGen().Sample(i, 30)).ToList();
        samples.Should().HaveCount(300);
        samples.Select(s => s.ArrInt).GroupBy(xs => xs.Length).Should().HaveCountGreaterThan(1);
        samples.Select(s => s.ArrString).GroupBy(xs => xs.Length).Should().HaveCountGreaterThan(1);
        samples.Select(s => s.BigInt).Distinct().Should().HaveCountGreaterThan(1);
        samples.Select(s => s.Bool).Distinct().Should().HaveCountGreaterThan(1);
        samples.Select(s => s.Byte).Distinct().Should().HaveCountGreaterThan(1);
        samples.Select(s => s.Complx).Distinct().Should().HaveCountGreaterThan(1);
        samples.Select(s => s.Decimal).Distinct().Should().HaveCountGreaterThan(1);
        samples.Select(s => s.Decimals).GroupBy(xs => xs.Count).Should().HaveCountGreaterThan(1);
        samples.Select(s => s.Double).Distinct().Should().HaveCountGreaterThan(1);
        samples.Select(s => s.Dt).Distinct().Should().HaveCountGreaterThan(1);
        samples.Select(s => s.Dto).Distinct().Should().HaveCountGreaterThan(1);
        samples.Select(s => s.Float).Distinct().Should().HaveCountGreaterThan(1);
        samples.Select(s => s.Floats).GroupBy(xs => xs.Count).Should().HaveCountGreaterThan(1);
        samples.Select(s => s.Guid).Distinct().Should().HaveCountGreaterThan(1);
        samples.Select(s => s.Int).Distinct().Should().HaveCountGreaterThan(1);
        samples.Select(s => s.Ints).GroupBy(xs => xs.Count).Should().HaveCountGreaterThan(1);
        samples.Select(s => s.Long).Distinct().Should().HaveCountGreaterThan(1);
        samples.Select(s => s.Longs).GroupBy(xs => xs.Count).Should().HaveCountGreaterThan(1);
        samples.Select(s => s.Obj).Distinct().Should().HaveCountGreaterThan(1);
        samples.Select(s => s.Short).Distinct().Should().HaveCountGreaterThan(1);
        samples.Select(s => s.String).Distinct().Should().HaveCountGreaterThan(1);
        samples.Select(s => s.Strings).GroupBy(xs => xs.Count()).Should().HaveCountGreaterThan(1);
        samples.Select(s => s.Ts).Distinct().Should().HaveCountGreaterThan(1);
    }

    [Property(Arbitrary = new[] { typeof(ConstantSUT) })]
    public static void ConstantSUTIsGenerated(SUT sut)
    {
        Assert.NotNull(sut);
    }

    [Fact]
    public static void ForConstantSUT_ManyInstancesCanBeGenerated_AllAreEqualToEachOther()
    {
        var samples = Enumerable.Range(0, 10).SelectMany(i => ConstantSUT.GetGen().Sample(i, 30)).ToList();
        samples.Should().HaveCount(300);
        samples.Select(s => s.ArrInt).Distinct().Should().HaveCount(1).And.OnlyContain(x => x == default(int[]));
        samples.Select(s => s.ArrString).Distinct().Should().HaveCount(1).And.OnlyContain(x => x == default(string[]));
        samples.Select(s => s.BigInt).Distinct().Should().HaveCount(1).And.OnlyContain(x => x == default(BigInteger));
        samples.Select(s => s.Bool).Distinct().Should().HaveCount(1).And.OnlyContain(x => x == default(bool));
        samples.Select(s => s.Byte).Distinct().Should().HaveCount(1).And.OnlyContain(x => x == default(byte));
        samples.Select(s => s.Complx).Distinct().Should().HaveCount(1).And.OnlyContain(x => x == default(Complex));
        samples.Select(s => s.Decimal).Distinct().Should().HaveCount(1).And.OnlyContain(x => x == default(decimal));
        samples.Select(s => s.Decimals).Distinct().Should().HaveCount(1).And.OnlyContain(x => x == default(ICollection<decimal>));
        samples.Select(s => s.Double).Distinct().Should().HaveCount(1).And.OnlyContain(x => x == default(double));
        samples.Select(s => s.Dt).Distinct().Should().HaveCount(1).And.OnlyContain(x => x == default(DateTime));
        samples.Select(s => s.Dto).Distinct().Should().HaveCount(1).And.OnlyContain(x => x == default(DateTimeOffset));
        samples.Select(s => s.Float).Distinct().Should().HaveCount(1).And.OnlyContain(x => x == default(float));
        samples.Select(s => s.Floats).Distinct().Should().HaveCount(1).And.OnlyContain(x => x == default(List<float>));
        samples.Select(s => s.Guid).Distinct().Should().HaveCount(1).And.OnlyContain(x => x == default(Guid));
        samples.Select(s => s.Int).Distinct().Should().HaveCount(1).And.OnlyContain(x => x == default(int));
        samples.Select(s => s.Ints).Distinct().Should().HaveCount(1).And.OnlyContain(x => x == default(IList<int>));
        samples.Select(s => s.Long).Distinct().Should().HaveCount(1).And.OnlyContain(x => x == default(long));
        samples.Select(s => s.Longs).Distinct().Should().HaveCount(1).And.OnlyContain(x => x == default(ISet<long>));
        samples.Select(s => s.Obj).Distinct().Should().HaveCount(1).And.OnlyContain(x => x == default(object));
        samples.Select(s => s.Short).Distinct().Should().HaveCount(1).And.OnlyContain(x => x == default(short));
        samples.Select(s => s.String).Distinct().Should().HaveCount(1).And.OnlyContain(x => x == default(string));
        samples.Select(s => s.Strings).Distinct().Should().HaveCount(1).And.OnlyContain(x => x == default(IEnumerable<string>));
        samples.Select(s => s.Ts).Distinct().Should().HaveCount(1).And.OnlyContain(x => x == default(TimeSpan));
    }

    [Property(Arbitrary = new[] { typeof(BasicSUTConstrainedInt) })]
    public static void ForBasicSUTConstrainedInt_IntIsPositive_EnumIsArbitrary(BasicSUT sut)
    {
        sut.Should().NotBeNull();
        sut.Enum.Should().BeOneOf(BasicEnum.State1, BasicEnum.State2, BasicEnum.State3);
        sut.Int.Should().BeGreaterThanOrEqualTo(0);
    }

    [Property(Arbitrary = new[] { typeof(BasicSUTConstrainedEnum) })]
    public static void ForBasicSUTConstrainedEnum_EnumIsNotState3_IntIsArbitrary(BasicSUT sut)
    {
        sut.Should().NotBeNull();
        sut.Enum.Should().BeOneOf(BasicEnum.State1, BasicEnum.State2);
    }
}