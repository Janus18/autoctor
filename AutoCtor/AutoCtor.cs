using AutoCtor.Lib;
using AutoCtor.Lib.Impl;
using FsCheck;

namespace AutoCtor;

public class AutoCtor<T> where T : class
{
    public ICollection<IPropertyGenerator> Properties { get; }

    public AutoCtor()
    {
        Properties = new List<IPropertyGenerator>();
    }

    public static AutoCtor<T> Builder()
    {
        return new AutoCtor<T>();
    }

    public AutoCtor<T> Having<TProperty>(string propertyName, Gen<TProperty> generator)
    {
        var propertyGenerator = ReflectionPropertyGenerator<T>.Cons<T, TProperty>(propertyName, generator);
        Properties.Add(propertyGenerator);
        return this;
    }

    public AutoCtor<T> HavingConstant<TProperty>(string propertyName, TProperty? value = default(TProperty))
    {
        var generator = Gen.Constant(value);
        var propertyGenerator = ReflectionPropertyGenerator<T>.Cons<T, TProperty>(propertyName, generator!);
        Properties.Add(propertyGenerator);
        return this;
    }

    public Gen<T> GetGenerator()
    {
        return AutoCtor.Lib.Static.TypeMapper.ClassGen<T>(Properties);
    }
}