using AutoCtor.Extensions;
using AutoCtor.Lib.Impl;
using FsCheck;
using System.Numerics;
using System.Reflection;

namespace AutoCtor.Lib.Static;

public static class TypeMapper
{
    public static Gen<T> GetGenFor<T>()
    {
        Type type = typeof(T);
        if (type.IsArray) return ArrayGen<T>();
        if (type.Is<BigInteger>()) return BigIntGen<T>();
        if (type.Is<byte>()) return ByteGen<T>();
        if (type.Is<bool>()) return BoolGen<T>();
        if (type.Is<char>()) return CharGen<T>();
        if (type.Is<Complex>()) return ComplexGen<T>();
        if (type.Is<DateTime>()) return DateTimeGen<T>();
        if (type.Is<DateTimeOffset>()) return DateTimeOffsetGen<T>();
        if (type.Is<decimal>()) return DecimalGen<T>();
        if (type.Is<double>()) return DoubleGen<T>();
        if (type.IsEnum) return EnumGen<T>();
        if (type.Is<float>()) return FloatGen<T>();
        if (type.Is<Guid>()) return GuidGen<T>();
        if (type.NameEquals(typeof(ICollection<>))) return CollectionsGen<T>(nameof(TypeMapper.ICollectionGen));
        if (type.NameEquals(typeof(IEnumerable<>))) return CollectionsGen<T>(nameof(TypeMapper.IEnumerableGen));
        if (type.NameEquals(typeof(IList<>))) return CollectionsGen<T>(nameof(TypeMapper.IListGen));
        if (type.Is<int>()) return IntGen<T>();
        if (type.NameEquals(typeof(ISet<>))) return CollectionsGen<T>(nameof(TypeMapper.ISetGen));
        if (type.NameEquals(typeof(List<>))) return CollectionsGen<T>(nameof(TypeMapper.ListGen));
        if (type.Is<long>()) return LongGen<T>();
        if (type.Is<object>()) return ObjectGen<T>();
        if (type.Is<short>()) return ShortGen<T>();
        if (type.Is<string>()) return StringGen<T>();
        if (type.Is<TimeSpan>()) return TimeSpanGen<T>();
        if (type.IsClass) return ClassGen<T>();
        throw new NotImplementedException($"Cannot create a generator for type {type.Name}");
    }

    private static Gen<T> ArrayGen<T>()
    {
        Type type = typeof(T);
        var method = (Arbitrary<T>)typeof(Arb.Default).GetMethods()
            .First(m => m.Name == nameof(Arb.Default.Array))
            .MakeGenericMethod(new[] { type.GetElementType()! })
            .Invoke(null, null)!;
        return method.Generator;
    }

    private static Gen<T> BoolGen<T>() =>
        Arb.Default.Bool().Generator.Select(b => (T)(object)b);

    private static Gen<T> BigIntGen<T>() =>
        Arb.Default.BigInt().Generator.Select(i => (T)(object)i);

    private static Gen<T> ByteGen<T>() =>
        Arb.Default.Byte().Generator.Select(i => (T)(object)i);

    private static Gen<T> CharGen<T>() =>
        Arb.Default.Char().Generator.Select(c => (T)(object)c);

    public static Gen<T> ClassGen<T>(ICollection<IPropertyGenerator>? generators = null)
    {
        var type = typeof(T);
        var props = type.GetProperties()
            .Where(p => p.SetMethod?.IsStatic == false && p.GetMethod?.IsStatic != true)
            .ToList();
        generators ??= new List<IPropertyGenerator>();
        var missingGenerators = props
            .Where(p => !generators.Any(g => p.Name == g.PropertyName()))
            .Select(p => ReflectionPropertyGenerator.Cons(p))
            .ToList();
        generators = generators.Concat(missingGenerators).ToList();
        T instance = Activator.CreateInstance<T>();
        var result = FsCheck.Gen.Constant<T>(instance);
        foreach (var generator in generators)
        {
            result = result.SelectMany(r => generator.ApplyGen(r));
        }
        return result!;
    }

    private static Gen<T> ComplexGen<T>() =>
        Arb.Default.Complex().Generator.Select(i => (T)(object)i);

    private static Gen<T> DateTimeGen<T>() =>
        Arb.Default.DateTime().Generator.Select(i => (T)(object)i);

    private static Gen<T> DateTimeOffsetGen<T>() =>
        Arb.Default.DateTimeOffset().Generator.Select(i => (T)(object)i);

    private static Gen<T> DecimalGen<T>() =>
        Arb.Default.Decimal().Generator.Select(d => (T)(object)d);

    private static Gen<T> DoubleGen<T>() =>
        Arb.Default.Float().Generator.Select(i => (T)(object)i);

    private static Gen<T> EnumGen<T>() =>
        Arb.Default.Derive<T>().Generator;

    private static Gen<T> FloatGen<T>() =>
        Arb.Default.Float32().Generator.Select(i => (T)(object)i);

    private static Gen<T> GuidGen<T>() =>
        Arb.Default.Guid().Generator.Select(x => (T)(object)x);

    private static Gen<T> ObjectGen<T>() =>
        Arb.Default.Object().Generator.Select(i => (T)(object)i);

    private static Gen<T> ShortGen<T>() =>
        Arb.Default.Int16().Generator.Select(i => (T)(object)i);

    private static Gen<T> CollectionsGen<T>(string methodName) =>
        (Gen<T>)typeof(TypeMapper)
            .GetMethod(methodName, BindingFlags.Static | BindingFlags.NonPublic)!
            .MakeGenericMethod(typeof(T).GenericTypeArguments)
            .Invoke(null, null)!;

    private static Gen<ICollection<T>> ICollectionGen<T>() =>
        Arb.Default.ICollection<T>().Generator;

    private static Gen<IEnumerable<T>> IEnumerableGen<T>() =>
        Arb.Default.IList<T>().Generator.Select(x => (IEnumerable<T>)x);

    private static Gen<IList<T>> IListGen<T>() =>
        Arb.Default.IList<T>().Generator;

    private static Gen<T> IntGen<T>() =>
        Arb.Default.Int32().Generator.Select(i => (T)(object)i);

    private static Gen<ISet<T>> ISetGen<T>() =>
        Arb.Default.Set<T>().Generator.Select(x => (ISet<T>)x.ToHashSet());

    private static Gen<List<T>> ListGen<T>() =>
        Arb.Default.List<T>().Generator;

    private static Gen<T> LongGen<T>() =>
        Arb.Default.Int64().Generator.Select(i => (T)(object)i);

    private static Gen<T> StringGen<T>() =>
        Arb.Default.String().Generator.Select(s => (T)(object)s);

    private static Gen<T> TimeSpanGen<T>() =>
        Arb.Default.TimeSpan().Generator.Select(i => (T)(object)i);
}
