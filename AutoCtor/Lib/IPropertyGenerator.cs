using FsCheck;

namespace AutoCtor.Lib;

public interface IPropertyGenerator
{
    string PropertyName();

    Gen<A> ApplyGen<A>(A instance);
}
