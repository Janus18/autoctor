using System.Reflection;
using AutoCtor.Exceptions;
using AutoCtor.Lib.Static;
using FsCheck;

namespace AutoCtor.Lib.Impl;

public static class ReflectionPropertyGenerator
{
    public static IPropertyGenerator Cons(PropertyInfo p)
    {
        var type = typeof(ReflectionPropertyGenerator<>);
        var genType = type.MakeGenericType(new[] { p.PropertyType });
        var cons = genType.GetConstructors().First();
        return (IPropertyGenerator)cons.Invoke(new[] { p });
    }
}

public class ReflectionPropertyGenerator<T> : IPropertyGenerator
{
    private readonly PropertyInfo propertyInfo;
    
    private readonly Gen<T> generator;

    public ReflectionPropertyGenerator(PropertyInfo propertyInfo)
    {
        this.propertyInfo = propertyInfo;
        generator = TypeMapper.GetGenFor<T>();
    }

    public ReflectionPropertyGenerator(PropertyInfo propertyInfo, Gen<T> generator)
    {
        this.propertyInfo = propertyInfo;
        this.generator = generator;
    }

    public static ReflectionPropertyGenerator<T2> Cons<T2>(PropertyInfo p)
    {
        return new ReflectionPropertyGenerator<T2>(p);
    }

    public static ReflectionPropertyGenerator<TProperty> Cons<TClass, TProperty>(
        string propertyName,
        Gen<TProperty> generator
    )
    {
        var propertyInfo = typeof(TClass).GetProperty(propertyName);
        if (propertyInfo == null)
            throw new PropertyNotFoundException(typeof(TClass), propertyName);
        return new ReflectionPropertyGenerator<TProperty>(propertyInfo, generator);
    }

    public string PropertyName()
    {
        return propertyInfo.Name;
    }

    public Gen<A> ApplyGen<A>(A instance)
    {
        return generator.Select(x =>
        {
            propertyInfo.SetValue(instance, x);
            return instance;
        });
    }
}
