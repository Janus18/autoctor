namespace AutoCtor.Exceptions;

public class PropertyNotFoundException : Exception
{
    public Type Type { get; }

    public string PropertyName { get; }

    public PropertyNotFoundException(Type type, string propertyName)
        : base($"A public property with name {propertyName} was not found in type {type}")
    {
        Type = type;
        PropertyName = propertyName;
    }
}
