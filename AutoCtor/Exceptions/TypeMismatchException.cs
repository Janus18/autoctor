namespace AutoCtor.Exceptions;

public class TypeMismatchException : Exception
{
    public Type ExpectedType { get; }

    public Type ActualType { get; }

    public TypeMismatchException(Type expected, Type actual)
        : base($"Type {expected} was expected, but actual type was {actual}")
    {
        ExpectedType = expected;
        ActualType = actual;
    }
}
