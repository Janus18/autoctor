namespace AutoCtor.Extensions;

public static class TypeExtensions
{
    /// <summary>
    /// Compare names of this type with t2's name. The namespace name is included,
    /// but no other info is. The method is useful to know if two generic types
    /// are the same without considering the argument types; for example, comparing
    /// List<int> and List<char> should return true.
    /// </summary>
    public static bool NameEquals(this Type t1, Type t2)
    {
        return t1.Name == t2.Name && t1.Namespace == t2.Namespace;
    }

    public static bool Is<T>(this Type t)
    {
        return typeof(T) == t;
    }
}